erlang-p1-xmpp (1.6.1-1) unstable; urgency=medium

  * New upstream version 1.6.1
  * Refreshed debian/patches/rebar.config.diff

 -- Julian Ribbeck <j.r@jugendhacker.de>  Wed, 25 Jan 2023 17:07:19 +0000

erlang-p1-xmpp (1.6.0-1) unstable; urgency=medium

  * New upstream version 1.6.0
  * Updated Erlang dependencies
  * Refreshed debian/patches/rebar.config.diff

 -- Philipp Huebner <debalance@debian.org>  Wed, 02 Nov 2022 15:26:49 +0100

erlang-p1-xmpp (1.5.8-1) unstable; urgency=medium

  * New upstream version 1.5.8
  * Updated Standards-Version: 4.6.1 (no changes needed)
  * Updated Erlang dependencies
  * Updated years in debian/copyright

 -- Philipp Huebner <debalance@debian.org>  Thu, 02 Jun 2022 13:19:22 +0200

erlang-p1-xmpp (1.5.6-1) unstable; urgency=medium

  [ Philipp Huebner ]
  * New upstream version 1.5.6
  * Updated Erlang dependencies
  * Updated debian/watch

  [ Debian Janitor ]
  * debian/copyright: use spaces rather than tabs to start continuation lines.
  * Set field Upstream-Contact in debian/copyright.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

 -- Philipp Huebner <debalance@debian.org>  Mon, 20 Dec 2021 19:52:24 +0100

erlang-p1-xmpp (1.5.4-1) unstable; urgency=medium

  * New upstream version 1.5.4
  * Updated Standards-Version: 4.6.0 (no changes needed)
  * Updated Erlang dependencies
  * Dropped patch now included upstream

 -- Philipp Huebner <debalance@debian.org>  Sun, 29 Aug 2021 22:18:51 +0200

erlang-p1-xmpp (1.5.2-3) unstable; urgency=medium

  * Added upstream patch to fix random crashes and weird behaviour with
    ejabberd 21.01 (Closes: #988115)

 -- Philipp Huebner <debalance@debian.org>  Thu, 13 May 2021 12:35:16 +0200

erlang-p1-xmpp (1.5.2-2) unstable; urgency=medium

  * Corrected Multi-Arch setting to "allowed"

 -- Philipp Huebner <debalance@debian.org>  Sun, 31 Jan 2021 19:43:57 +0100

erlang-p1-xmpp (1.5.2-1) unstable; urgency=medium

  * New upstream version 1.5.2
  * Added 'Multi-Arch: same' in debian/control
  * Updated Erlang dependencies
  * Updated years in debian/copyright

 -- Philipp Huebner <debalance@debian.org>  Sat, 30 Jan 2021 19:17:11 +0100

erlang-p1-xmpp (1.5.1-1) unstable; urgency=medium

  * New upstream version 1.5.1
  * Updated Standards-Version: 4.5.1 (no changes needed)
  * Updated Erlang dependencies
  * Updated version of debian/watch: 4
  * Updated debhelper compat version: 13
  * Updated lintian overrides
  * Added debian/erlang-p1-xmpp.install
  * Refreshed debian/patches/fix-includes.diff

 -- Philipp Huebner <debalance@debian.org>  Sat, 26 Dec 2020 01:08:33 +0100

erlang-p1-xmpp (1.4.9-1) unstable; urgency=medium

  * New upstream version 1.4.9
  * Updated Erlang dependencies

 -- Philipp Huebner <debalance@debian.org>  Sun, 02 Aug 2020 18:17:39 +0200

erlang-p1-xmpp (1.4.8-1) unstable; urgency=medium

  * New upstream version 1.4.8
  * Updated Erlang dependencies

 -- Philipp Huebner <debalance@debian.org>  Sun, 26 Jul 2020 22:38:43 +0200

erlang-p1-xmpp (1.4.6-1) unstable; urgency=medium

  * New upstream version 1.4.6
  * Updated Erlang dependencies

 -- Philipp Huebner <debalance@debian.org>  Fri, 01 May 2020 13:57:08 +0200

erlang-p1-xmpp (1.4.5-1) unstable; urgency=medium

  * New upstream version 1.4.5
  * Updated Erlang dependencies

 -- Philipp Huebner <debalance@debian.org>  Wed, 18 Mar 2020 13:59:39 +0100

erlang-p1-xmpp (1.4.4-1) unstable; urgency=medium

  * New upstream version 1.4.4
  * Updated Standards-Version: 4.5.0 (no changes needed)
  * Updated Erlang dependencies
  * Updated years in debian/copyright
  * Rules-Requires-Root: no

 -- Philipp Huebner <debalance@debian.org>  Sat, 08 Feb 2020 13:39:23 +0100

erlang-p1-xmpp (1.4.2-1) unstable; urgency=medium

  * New upstream version 1.4.2
  * Updated Erlang dependencies
  * Updated Standards-Version: 4.4.1 (no changes needed)
  * Fixed typo in long package description.
  * Set 'Rules-Requires-Root: no' in debian/control

 -- Philipp Huebner <debalance@debian.org>  Mon, 11 Nov 2019 16:20:32 +0100

erlang-p1-xmpp (1.4.0-1) unstable; urgency=medium

  * New upstream version 1.4.0
  * Updated Erlang dependencies

 -- Philipp Huebner <debalance@debian.org>  Sat, 10 Aug 2019 14:49:00 +0200

erlang-p1-xmpp (1.3.4-1) unstable; urgency=medium

  * New upstream version 1.3.4
  * Updated Erlang dependencies
  * Updated Standards-Version: 4.4.0 (no changes needed)
  * debian/rules: export ERL_COMPILER_OPTIONS=deterministic
  * Updated debhelper compat version: 12

 -- Philipp Huebner <debalance@debian.org>  Wed, 24 Jul 2019 13:04:59 +0200

erlang-p1-xmpp (1.2.8-1) unstable; urgency=medium

  * New upstream version 1.2.8
  * Updated Erlang dependencies
  * Updated Standards-Version: 4.3.0 (no changes needed)
  * Updated years in debian/copyright

 -- Philipp Huebner <debalance@debian.org>  Tue, 01 Jan 2019 22:54:31 +0100

erlang-p1-xmpp (1.2.5-1) unstable; urgency=medium

  * New upstream version 1.2.5
  * Enabled DH_VERBOSE in debian/rules
  * Updated Standards-Version: 4.2.1 (no changes needed)

 -- Philipp Huebner <debalance@debian.org>  Sun, 07 Oct 2018 16:26:50 +0200

erlang-p1-xmpp (1.2.2-1) unstable; urgency=medium

  * New upstream version 1.2.2
  * Added debian/upstream/metadata

 -- Philipp Huebner <debalance@debian.org>  Wed, 04 Jul 2018 16:25:12 +0200

erlang-p1-xmpp (1.1.21-1) unstable; urgency=medium

  * New upstream version 1.1.21
  * Updated Standards-Version: 4.1.4 (no changes needed)

 -- Philipp Huebner <debalance@debian.org>  Wed, 09 May 2018 17:20:11 +0200

erlang-p1-xmpp (1.1.20-1) unstable; urgency=medium

  * New upstream version 1.1.20
  * Switched to debhelper 11

 -- Philipp Huebner <debalance@debian.org>  Tue, 27 Mar 2018 23:25:58 +0200

erlang-p1-xmpp (1.1.19-1) unstable; urgency=medium

  * New upstream version 1.1.19
  * (Build-)Depend on erlang-p1-xml (>=1.1.28)

 -- Philipp Huebner <debalance@debian.org>  Sat, 13 Jan 2018 16:45:42 +0100

erlang-p1-xmpp (1.1.17-1) unstable; urgency=medium

  * Set Maintainer: Ejabberd Packaging Team <ejabberd@packages.debian.org>
  * Set Uploaders: Philipp Huebner <debalance@debian.org>
  * Updated Vcs-* fields in debian/control for salsa.debian.org
  * Updated years in debian/copyright
  * Updated Standards-Version: 4.1.3 (no changes needed)
  * New upstream version 1.1.17
  * (Build-)Depend on erlang-p1-xml (>=1.1.26)

 -- Philipp Huebner <debalance@debian.org>  Thu, 04 Jan 2018 11:38:02 +0100

erlang-p1-xmpp (1.1.16-1) unstable; urgency=medium

  * New upstream version 1.1.16
  * Updated Standards-Version: 4.1.1 (no changes needed)

 -- Philipp Huebner <debalance@debian.org>  Mon, 27 Nov 2017 20:18:41 +0100

erlang-p1-xmpp (1.1.14-2) unstable; urgency=high

  * (Build-)Depend on erlang-base (>= 1:19.2)

 -- Philipp Huebner <debalance@debian.org>  Mon, 18 Sep 2017 08:28:48 +0200

erlang-p1-xmpp (1.1.14-1) unstable; urgency=medium

  * New upstream version 1.1.14
  * Updated Standards-Version: 4.1.0 (no changes needed)

 -- Philipp Huebner <debalance@debian.org>  Sat, 02 Sep 2017 16:58:30 +0200

erlang-p1-xmpp (1.1.13-1) unstable; urgency=medium

  * New upstream version 1.1.13
  * Updated Standards-Version: 4.0.0 (no changes needed)

 -- Philipp Huebner <debalance@debian.org>  Tue, 11 Jul 2017 17:10:12 +0200

erlang-p1-xmpp (1.1.9-1~exp1) experimental; urgency=medium

  * Initial release (Closes: #858969)

 -- Philipp Huebner <debalance@debian.org>  Sat, 15 Apr 2017 12:01:44 +0200
